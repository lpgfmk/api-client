<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;


Route::get('/', function () {
	$query = http_build_query([
		'client_id' => '5',
		'redirect_uri' => 'http://api-client.local/callback',
		'response_type' => 'code',
		'scope' => '',
	]);

	return redirect('http://api-auth.local/oauth/authorize?'.$query);
});

Route::get('/callback', function (Request $request) {
	$http = new GuzzleHttp\Client;

	$response = $http->post('http://api-auth.local/oauth/token', [
		'form_params' => [
			'grant_type' => 'authorization_code',
			'client_id' => '5',
			'client_secret' => 'cHfCmLgWXttLNiV2TNVGeH9r7slrCA9DyiFr7EBg',
			'redirect_uri' => 'http://api-client.local/callback',
			'code' => $request->code,
		],
	]);

	return json_decode((string) $response->getBody(), true);
});
